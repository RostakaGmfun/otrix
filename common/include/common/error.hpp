#ifndef OTRIX_ERROR_HPP
#define OTRIX_ERROR_HPP

namespace otrix
{

enum class error
{
    ok,
    nodev,
    nomem,
    inval
};

} // namespace otrix

#endif // OTRIX_ERROR_HPP
