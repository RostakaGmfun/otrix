#ifndef OTRIX_ERROR_H
#define OTRIX_ERROR_H

typedef enum
{
    EOK,
    ENODEV,
    ENOMEM,
    EINVAL,
} error_t;

#endif // OTRIX_ERROR_H
